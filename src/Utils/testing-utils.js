import { applyMiddleware, createStore } from 'redux';
import rootReducer from './../reducers';
import { middlewares } from './../createStore';

export const findByTestAttr = (component, attr) => {
    const wrapper = component.find(`[data-test='${attr}']`);
    return wrapper;
  }
export const testStore = (initialState) => {
  const createStoreWithMiddleware = applyMiddleware(...middlewares)(createStore);
  return createStoreWithMiddleware(rootReducer, initialState);
};
export const simulateChangeOnInput = (wrapper, inputSelector, name, newValue) => {
  const input = wrapper.find(inputSelector);
  input.simulate('change', {
      target:{ name, value: newValue}
  })
  return wrapper.find(inputSelector);
}