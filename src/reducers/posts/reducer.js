import { types } from '../../actions/types'
const initialState = {
    posts: [],
    loading: true,
    success:false,
 
};

// eslint-disable-next-line
export default (state=initialState, action) => { 
    switch(action.type) {
        case types.GET_POSTS :
            return { 
                ...state,
               posts: action.payload,
               loading:false,
               success:true

            }
        default:
            return state
   }
 }