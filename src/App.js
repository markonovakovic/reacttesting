import './App.css';
import {
  Switch,
  Route
} from "react-router-dom"

import Home  from './pages/home'
import About from './pages/about'
import Signup from './pages/signup'

function App() {
  return (
    <Switch>
      <Route path="/" component={Home} exact />
      <Route path="/about" component={About} />
      <Route path="/signup" component={Signup} />
    </Switch>
   

  );
}

export default App;
