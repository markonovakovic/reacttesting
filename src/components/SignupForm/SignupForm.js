import React, { useState } from 'react'

const defaultData = {
    fullName:'',
    email:'',
    favoritAuthor:2
};

 const SignupForm = () => {
     const [data, setData] = useState(defaultData)
    const handleSubmit = (e) =>  {
        e.preventDefault();
       console.log(data);
    }

    const changeHandler = ({target}) => {
        const {name, value} = target;
        setData(oldData =>  ({...oldData,[name]:value }))
        console.log(target.name);
    }
    return (
        <div>
            {JSON.stringify(data)}
             <form onSubmit={handleSubmit}>
                   <div className="mb-3">
                        <label htmlFor="fullName" className="form-label">Full Name</label>
                        <input 
                            type="text"
                            id="fullName" 
                            name="fullName" 
                            className="form-control" 
                            value={data.fullName} 
                            onChange={changeHandler}/>
                    </div> 
                    <div className="mb-3">
                        <label htmlFor="exampleInputEmail1" className="form-label">Email address</label>
                        <input 
                            type="email" 
                            name="email" 
                            className="form-control" 
                            id="exampleInputEmail1" 
                            aria-describedby="emailHelp" 
                            value={data.email}
                            onChange={changeHandler}
                            />
                        <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div>
                    </div>
                    <select className="form-select" name="favoritAuthor" value={data.favoritAuthor} aria-label="Default select example" onChange={changeHandler}>
                        <option >Open this select menu</option>
                        <option value="1">One</option>
                        <option  defaultValue={2} value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                    <button type="submit" className="btn btn-primary">Submit</button>

                </form>
        </div>
    )
}
export default SignupForm