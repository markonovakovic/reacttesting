import React from 'react'
import { shallow, mount } from 'enzyme'
import SignupForm from './SignupForm'
import { simulateChangeOnInput } from '../../Utils/testing-utils'



describe('SignupForm', () => {
    let wrapper;
    beforeAll(() => { 
        wrapper =  mount(<SignupForm />)

    })
    it('Snipshot of SignupForm', () => {
        
        expect(wrapper).toMatchSnapshot()
    });
    it('has a default favorit writer selected', () => {
        const selectAuthors  = wrapper.find('select');
        expect(selectAuthors.props().value).toEqual(2)
    });
/*   First Options

    it('lets me fill out the form and reset it', () => {
        let nameInput = wrapper.find('input').first();
        nameInput.simulate('change', {
            target: {name: 'fullName', value: 'Jasar'}
        });
        nameInput = wrapper.find('input').first();
        console.log(nameInput)
        expect(nameInput.props().value).toEqual('Jasar')
    });
     */
    // Second option
    it('lets me fill out the form and reset it', () => {
     
        const fullNameInput = simulateChangeOnInput(wrapper, 'input#fullName','fullName', 'jasarJe dobar')
        const emailInput = simulateChangeOnInput(wrapper, 'input#fullName','fullName', 'jasar@mail.ru')
        const favoritAuthor = simulateChangeOnInput(wrapper, 'input#fullName','fullName', 3)
   
        expect(fullNameInput.props().value).toEqual('jasarJe dobar')
        expect(emailInput.props().value).toEqual('jasar@mail.ru')
        expect(favoritAuthor.props().value).toEqual(3)
    });
    
})

/* 

 describe('the user does not populate the input field', () => {

    it('should display an error', () => {
      const form = wrapper.find('form').first();
      form.simulate('submit');
      expect(
        wrapper.find('p.error').first().text()
      ).toBe('Please enter your name.');
    });

  });

    // all other code omitted
  // bear in mind I am shallow rendering the component

  describe('the user does not populate the input field', () => {

    it('should display an error', () => {
      const form = wrapper.find('form').first();
      form.simulate('submit', {
        preventDefault: () => {
        },
        // below I am trying to set the value of the name field
        target: [
          {
            value: '',
          }
        ],
      });
      expect(
        wrapper.text()
      ).toBe('Please enter your name.');
    });

  });

*/