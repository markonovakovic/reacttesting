import React from 'react'
import { shallow } from 'enzyme'
import Navbar from './Navbar';

const links = [
  {title:'Home', path:'/'}
]
const setUp = (props={}) => {
  const component = shallow(<Navbar {...props} />);

  return component;
}

const findByTestAttr = (component, attr) => {
  const wrapper = component.find(`[data-test='${attr}']`);
  return wrapper;
}
describe('Navbar Component',() => {

  let component;
  
  beforeEach(() => {
    component = setUp(links);
  })
  describe('Nested', () => {
    it('should render without errors', () => {
      // console.log(component.debug())
      const wrapper = findByTestAttr(component, 'navbar');

      expect(wrapper.length).toBe(1);
    })
  // describe('passing props', () => {
  //    const navbarWrapper = mount(<Navbar  links={links} />);
  //    it('accepts header links props', () => {
  //      expect(navbarWrapper.props().links).toEqual(links)
  //    })

  // })

  describe('logic', () => {
    const wrapper = shallow(<Navbar  links={links} />);
    wrapper.find('.login-btn').simulate('click');

    it('btn click - update title', () => {
      const text = wrapper.find('.navbar-brand').text()
      expect(text).toEqual('Tralala')
    })
  })
  

    
    it('renders Nav component header without crashing', () => {

      const header = (<h2 data-test="navbar-title" className="navbar-brand">ReactTest</h2>);
      expect(component.contains(header)).toEqual(true)
    })
    it('render text of Navbar title', () => {
      const text = findByTestAttr(component, 'navbar-title').text()

      expect(text).toEqual('ReactTest')
    })
    it('renders login button', () => {
      const button =  component.find('.login-btn');
      expect(button).not.toBeNull()
    })
    it('renders login button text', () => {
      const button =  component.find('.login-btn').text();
      expect(button).toEqual('Login')
    })

    it('should click button element and update title', () => {
      const wrapper =  shallow(<Navbar links={links} />)
      const button = wrapper.find('.login-btn');
 
      button.simulate('click');
      const text = wrapper.find('.navbar-brand').text();
      expect(text).toEqual('Tralala');
  
   })
    it('should click Button component and update title', () => {
      const wrapper =  shallow(<Navbar links={links} />)
      const button = wrapper.find('Button');
 
      button.invoke('handleClick')()
      const text = wrapper.find('.navbar-brand').text();
      expect(text).toEqual('Sahbaz');
      
    
    })
  })

 
})