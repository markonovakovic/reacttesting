import React, { useState } from 'react'
import {
  Link
} from "react-router-dom";
import { Button } from '../../UI'
const Navbar = ({links=[]}) => {
  const [ navbarTitle, setNavbarTitle] = useState('ReactTest')

  const updateTitle =  () => {
    setNavbarTitle('fafafa')
  }
  return (
    <nav data-test="navbar" className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container-fluid">
        <h2 data-test="navbar-title" className="navbar-brand">{navbarTitle}</h2>
        <button data-test="hanburger-btn" className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse mx-5" id="navbarNavDropdown">
          <ul className="navbar-nav">
            { links.length > 0 && 
              links.map((link, index) => {
                  return (
                    <li key={index} className="nav-item">
                       <Link className="nav-link" to={link.path}>{link.title}</Link>
                  </li>
                  )
              })
            }
            <li>
              <Button className="moje dugme" text="Click" handleClick={() => setNavbarTitle('Sahbaz')} />
              <button data-test="login-btn" onClick={() => setNavbarTitle('Tralala')} className="login-btn">Login</button></li>
          </ul>
        </div>
      </div>
    </nav>
 
  )
}

export default Navbar
