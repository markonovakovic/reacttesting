import React, { useState, useEffect, useCallback, useMemo } from 'react'
import { useSelector, useDispatch } from 'react-redux' 
import { fetchPosts } from '../../../actions/posts';
import { Button, Search } from '../../UI'
import {TableHead, TableRow} from '../Table';

const tableHeadStyle = { 
    background:'green'
}
const PostList = () => {
    console.log('postList')
    const { posts } = useSelector(state => state.posts);
    const [ searchText, setSearchText] = useState('')
    const [text2, setText2] = useState('')
    const dispatch = useDispatch()

  
    // useEffect(() => {
    //     dispatch(fetchPosts())
    // },[dispatch]) 
    const searchCb = useCallback((text) => {
        
        setSearchText(text)
    },[setSearchText])
    //     dispatch(fetchPosts())
    // },[dispatch]) 
    const text2Cb = useCallback((e) => {
        
        setText2(e.target.value)
    },[setText2])
    const textMemo = useMemo(() => searchText,[searchText])
    const handleFetchPosts = () => {
        dispatch(fetchPosts())
    }

    const text2Memo = useMemo(() => text2,[text2])

    const tableHeadMemo =  useMemo(() =>{
        return <TableHead />
    },[])
    return (
        <div>
            <Button text="fetch data" className="btn-sm btn-secondary" handleClick={handleFetchPosts} />
                 {textMemo}
            <div className="row">
                <div className="col-sm-4">
                <Search
                    value={textMemo}
                    onChange={(event) => {
                        searchCb(event.target.value)
                       
                    }}
                />
                <input type="text" name="text2" value={text2Memo} onChange={text2Cb} placeholder="text2 " />
                </div>
            </div>
          
            <table className="table">
                {tableHeadMemo}
            {/* <TableHead title={'textMemo'} style={tableHeadStyle} /> */}
            <tbody>
              {
                  posts.length > 0 &&
                  posts.map(post => {
                      return <TableRow key={post.id} {...post} />
                  })
              }
            </tbody>
            </table>
        </div>
    )
}

export default PostList
