import React, {memo} from 'react'

const TableRow = memo(() => {
    console.log('tableRow');
    return (
        <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
        </tr>
    )
})

export default TableRow
