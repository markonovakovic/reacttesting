import React from 'react'

const Headline = ({title,description}) => {
    return (
        <div data-test="headlineComponent">
            <h1 data-test="title">{title}</h1>

            { description && <p data-test="description">{description}</p> }
        </div>
    )
}


export default Headline
