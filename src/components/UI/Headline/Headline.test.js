import React from 'react'
import { mount, shallow }  from 'enzyme'

import Headline from './Headline';


import { findByTestAttr } from '../../../Utils/testing-utils'


export const setUp = (props={}) => {
    const component = shallow(<Headline {...props} />);
  
    return component;
}

describe('Headline Component', () => {
    describe('Have props', () => {
        let wrapper;
        beforeEach(() => {
            const props = {
                title: 'About us',
                description:'My Description'
            }

            wrapper = setUp(props);
        })

        it('Should render without errors', () => {
            const component = findByTestAttr(wrapper, 'headlineComponent');
            expect(component.length).toBe(1)
        })
        it('Should render H1' , () => {
            const h1 = findByTestAttr(wrapper, 'title').text();
            expect(h1).toEqual('About us')
        })

    });

    describe('Passing props', () => {
        const headlineProps = {
            title:"helo man",
            description:'Test'
        }
        const headlineWrapper = mount(<Headline title={headlineProps.title} />)
        it('accepts header title props', () => {
           expect(headlineWrapper.props().title).toEqual(headlineProps.title)
        })
    })

    describe('Have NO props', () => {
        let wrapper;

        beforeEach(() => {
            wrapper = setUp();
        })
        it('Should not render h1', () => {
            const title = findByTestAttr(wrapper, 'title');
            expect(title.length).toBe(1);
        })
    }) 
    
})