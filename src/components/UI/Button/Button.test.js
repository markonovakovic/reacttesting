import React from 'react'
import { findByTestAttr } from '../../../Utils/testing-utils'
import Button from './Button'
import { mount, shallow } from 'enzyme'

describe('<Button />', () => {
    describe('Checking PropTypes', () => {
       it('Should handleChange when value foo is passed', () => {

         const expectedProps = {
            text: 'Example ButtonText', 
            className:"moje dugme",
            handleClick: () =>  {

            },
         }
        const props = mount(<Button {...expectedProps} />).props();
         //  console.log(props);
          expect(3).toBe(3);
       })
    })

   describe('Renders', () => {
      let wrapper;
      let mockFunc;

      beforeEach(() => {
         mockFunc = jest.fn();
            const props = {
               text:"My Btn",
               handleClick: mockFunc
            }
            wrapper = shallow(<Button {...props} handleClick={mockFunc} />)
            // console.log(mockFunc.mock)
       })

       it('Should Render a button', () => {
          const button = findByTestAttr(wrapper, 'button');
          expect(button.length).toBe(1)
       })

       it('Should emit callback on click event', () => {
        //  const button =wrapper.find(`[data-test='button']`);
         //  button.simulate('click');
        //  console.log(mockFunc.mock.calls.length, 'colls')
       //  expect(mockFunc).toBeCalledTimes(2)
       })
    })
})