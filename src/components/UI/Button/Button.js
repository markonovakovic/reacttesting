import React from 'react'
import PropTypes from 'prop-types'

const Button = ({children,className, text, handleClick, ...props}) => {
    const onClickEvent = () => {
        handleClick()
    }
    return (
        <button
           data-test="button" 
           className={`${className} btn btn-primary`} 
           onClick={onClickEvent}
           {...props}
           >
            {text}
        </button>
    )
}

Button.propTypes = {
    text:PropTypes.string,
    handleChange: PropTypes.func,
    handleBlur: PropTypes.func,
    handleSubmit: PropTypes.func,
    handleClick: PropTypes.func,
    
}

export default Button
