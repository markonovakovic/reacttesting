import React, {memo} from 'react'
import PropTypes from 'prop-types'

function Search({value, onChange}) {
    console.log('search');
    return (
        <>
             <input type="text" placeholder="search" name="search" value={value} onChange={onChange} /> 
        </>
    )
}

Search.propTypes  = {
   onChange: PropTypes.func.isRequired,
   value:PropTypes.string.isRequired
}
export default memo(Search)
