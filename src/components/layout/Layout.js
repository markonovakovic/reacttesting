import React from 'react'
import { Navbar } from '../common'
const links = [
    {title:'Home', path:'/'},
    {title:'About', path:'/about'},
    {title:'Signup', path:'/signup'}
]
const Layout = ({children}) => {
    return (
        <div>
            <Navbar links={links} />
            <main className="main container">
                {children}
            </main>
        </div>
    )
}

export default Layout
