import React from 'react';
import { shallow } from 'enzyme'
import TestApp from './TestApp';
import fetchMock from 'fetch-mock';
import moxios from 'moxios';
import * as posts from '../../actions/posts';
describe('should test <TestApp /> component', () => {

    // let wrapper;

    // beforeAll(() => {
    //     wrapper = shallow(<TestApp />);
    // })

    it('should call handleClick method', () => {
        const wrapper = shallow(<TestApp />)
        const button = wrapper.find('Button');
        button.invoke('onClick')()
        expect(1).toEqual(1);
         
       
    })
    
    
});

const nextTick = async () => {
    return new Promise(resolve => {
        setTimeout(resolve,0);
    })
}

const dummyPost = [{
    id:1,
    title:'Dummy title',
    body:'Dummy body'
}]

const expectedState = [{
    title: 'Example title 1',
    body: 'Some Text'
},{
    title: 'Example title 2',
    body: 'Some Text'
},{
    title: 'Example title 3',
    body: 'Some Text'
}];

describe('Mock  Test',() => {
    beforeEach(() => {
        moxios.install();
    });

    afterEach(() => {
        moxios.uninstall();
        jest.clearAllMocks();
    });

    it('show the loading text and then the data once it has been fetched', async () => {
        const url = "https://jsonplaceholder.typicode.com/posts"
        jest
           .spyOn(posts, 'fetchPosts')
           .mockImplementation(() => {
               return Promise.resolve(expectedState)
            })
    //     const wrapper = shallow(<TestApp />);

    //     fetchMock.getOnce(url,  {
    //             status:200,
    //             body:expectedState
    //         });
    //    // const response = await fetchPosts();

    //     await expect(fetchPosts()).resolves.toEqual(expectedState);
        // const setState = jest.fn();
        // const useStateSpy = jest.spyOn(React, 'useState')
        // useStateSpy.mockImplementation((init) => [init, setState]);
       
        // fetchMock.getOnce(url, {
        //     status:200,
        //     body:dummyPostw
        // })
     //   expect(wrapper.find('.loading').text()).toContain('Loading')
      //  setState(dummyPost)
        // await nextTick();
        // wrapper.update();

    //   expect(wrapper.find('PostTable')).toBeTruthy()
             // console.log(setState,'postwrapp')
      //  expect(setState).toHaveBeenCalledWith(dummyPost);
     
    })

})