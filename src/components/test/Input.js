import React from "react";

const Input = React.memo(({onChange,value, ...props}) => {

  return (
    <div className="input-component">
      <input
        type="text"
        value={value}
        className="form-control"
        aria-label="Text input with checkbox"
        // style={{ width: "200px" }}
        onChange={onChange}
        {...props}
      />
    </div>
  );
});

export default Input;
