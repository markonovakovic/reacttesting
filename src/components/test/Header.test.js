import React from 'react';
import { shallow } from'enzyme'
import Header from './Header';

test('should test Header without porps', () => {
    const wrapper = shallow(<Header />);
    expect(wrapper).toMatchSnapshot();
})

test('Should test Header component', () => {
    const wrapper = shallow(<Header title="hello bajau!" style={{background:'red'}} />);
    expect(wrapper).toMatchSnapshot();

})