import React from 'react'
import PropTypes from 'prop-types'


const Button = ({onClick,title, ...props}) => {
    return (
        <div className="button-component">
                <button onClick={onClick} {...props}> { title || 'Show Table'}</button>
        </div>
    )
}

Button.propTypes = {
    onClick: PropTypes.func
}

export default Button
