import React from 'react'

const Header = ({title, ...props}) => {
    return (
        <div className="header" {...props}>
            <h1>{title || "Default Text Header"}</h1>
        </div>
    )
}

export default Header
