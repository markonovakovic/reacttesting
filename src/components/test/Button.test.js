import React from 'react';
import { shallow } from'enzyme'
import Button from './Button';

test('should test Header without porps', () => {
    const wrapper = shallow(<Button title="Add Post" />);
    expect(wrapper).toMatchSnapshot();
})

test('Should handle onClick handler', () => {
    const onClickSpy = jest.fn();
    const wrapper = shallow(<Button onClick={onClickSpy} />);
    wrapper.find('button').simulate('click');

    expect(onClickSpy).toHaveBeenCalled();

})