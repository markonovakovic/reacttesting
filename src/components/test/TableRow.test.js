import React from 'react';
import { shallow } from 'enzyme'
import TableRow from './TableRow';


describe('should test <TableRow /> component', () => {

    test('should test PostRow component with data', () => {
        const postItem = {id: 1, title:'Novi post', body:'Da vidimo sta se sprema'}
        const wrapper = shallow(<TableRow key={postItem.id} item={postItem} />)
     

        expect(wrapper).toMatchSnapshot();
    })
    
    
})