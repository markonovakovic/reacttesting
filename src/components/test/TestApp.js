import React, { useState, useEffect, useCallback } from "react";
import "./style.css";

import Header from "./Header";
import PostTable from "./PostTable";
import Input from "./Input";
import Button from "./Button";

const TestApp = (props) => {
  const [posts, setPosts] = useState([]);
  const [newTitle, setNewTitle] = useState("");
  const [test] = useState("Testtt");

  useEffect(() => {
    getAllPosts();
  }, []);

  const getAllPosts = async () => {
    try {
      const result = await fetch("https://jsonplaceholder.typicode.com/posts");
      const json = await result.json();

      if (result.status === 200) {
        setPosts(json);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const setNewPostCb = useCallback(
    (e) => {
      setNewTitle((oldTitle) => e.target.value);
    },
    [setNewTitle]
  );

  const handleClick = () => {
    // console.log(newTitle);
    const newPost = {
      id: posts.length + 1,
      title: newTitle,
      body: "some text",
    };
    setPosts((oldPosts) => [newPost, ...oldPosts]);
    setNewTitle("");
  };
  const newTitleMemo = React.useMemo(() => {
    return newTitle;
  }, [newTitle]);

  const handleDelete = (id) => {
    console.log(id);
  };

  return (
    <div>
      Header Component
      <Header />
      Input Component
      <Input onChange={setNewPostCb} value={newTitleMemo} />
      Button Component
      <Button onClick={handleClick} />
      PostList Component
      {posts.length === 0 ? (
        <p className="loading">Loading...</p>
      ) : (
        <div className="post-list">
         
          <PostTable posts={posts} handleDelete={handleDelete} /> *
        </div>
      )}
    </div>
  );
};

export default TestApp;
