import React from 'react';


const TableRow = ({item, handleDelete}) => {
    return(
        <tr>
        <td>{item.id}</td>
        <td>{item.title}</td>
        <td>{item.body}</td>
        <td><button className="btn btn-danger" onClick={()=> handleDelete(item.id)}>Delete</button></td>
      </tr>
    )
}


export default TableRow;