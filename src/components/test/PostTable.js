import React from "react";
import PropTypes from 'prop-types';
import TableRow from './TableRow';

const PostTable = ({posts,handleDelete}) => {
  if(posts.length === 0 ) return <div>No have posts</div>
  return (
    <div className="post-list">
      <table>
        <thead>
          <tr>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Savings</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
            { posts && posts.map(item => {
                return <TableRow key={item.id} item={item} handleDelete={handleDelete} />
            })
              
            }
         
        
        </tbody>
      </table>
    </div>
  );
};

PostTable.defaultProps = {
    posts:[]
}

PostTable.propTypes = {
    posts: PropTypes.array.isRequired
};

export default PostTable;
