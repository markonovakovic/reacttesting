import React from 'react';
import { shallow } from 'enzyme'
import PostTable from './PostTable';
import renderer from 'react-test-renderer' // eslint-disable-line

describe('should test <PostTable /> component', () => {

    it('should test default state of empty array', () => {  
        const wrapper = shallow(<PostTable posts={[]} />)
        expect(wrapper).toMatchSnapshot()
    });

    it('should test PostTable component with  list of posts', () => {
        const posts = [
            {id:1, title:'Novi post', body:'Da vidimo sta se sprema'},
            {id:2, title:'Drugi  post', body:'Nad Kraljevom ziva vatra seva!'},
        ]
        const wrapper = shallow(<PostTable key="1" posts={posts} />)
       // const instance = wrapper.instance();
       // console.log(instance)
        expect(wrapper).toMatchSnapshot();
    })
    
   
    
})