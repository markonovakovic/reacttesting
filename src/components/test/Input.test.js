import React from 'react';
import { shallow } from 'enzyme'
import Input from './Input';

describe('<Input /> component', () => {
   
   it('Should render Input component', () => {
       const onChange = jest.fn();
       const value = 'Hello'
       const wrapper = shallow(<Input onChange={onChange}    value={value} />);

      // let text = wrapper.find('input').value();
       expect(wrapper).toMatchSnapshot()
   })

   it('Should call onChange function', () => {
    const onChange = jest.fn();
    const value = 'Hello'
    const wrapper = shallow(<Input onChange={onChange}  value={value} style={{width:'300px', padding:'4px'}}/>);
    wrapper.find('input').simulate('change');
    

    expect(onChange).toHaveBeenCalled()
   })
})