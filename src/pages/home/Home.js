import React from 'react'
import  Layout  from  '../../components/layout'
import  { Headline }  from '../../components/UI'
import PostList from '../../components/test/TestApp'
const Home = () => {
    return (
        <Layout>
             <>
              <Headline title="Home Page" />
              <PostList />
             </>
        </Layout>
    )
}

export default Home
