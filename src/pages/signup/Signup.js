import React from 'react'
import  Layout  from  '../../components/layout'
import SignupForm from '../../components/SignupForm/SignupForm'
const Signup = () => {
    return (
        <Layout>
            <SignupForm />
        </Layout>
    )
}

export default Signup
