import React from 'react'
import { shallow } from 'enzyme'
import Navbar from './App';

const setUp = (props={}) => {
  const component = shallow(<Navbar {...props} />);

  return component;
}

describe('Navbar Component',() => {

  let component;

  beforeEach(() => {
    component = setUp();
  })

  it("render Nav component without crashing",  () => {
        setUp();   
  })

  describe('Nested', () => {
    it('should render without errors', () => {
      // console.log(component.debug())
      const wrapper = component.find('.App-link');
      expect(wrapper).not.toBeNull()
    })

  })

  describe('should array have milk', () => {
    const shoppingList = [
      'diapers',
      'kleenex',
      'trash bags',
      'paper towels',
      'milk',
    ];

    test('the shopping list has milk on it', () => {
      expect(shoppingList).toContain('milk');
      expect(new Set(shoppingList)).toContain('milk');
    });
    
  })
})