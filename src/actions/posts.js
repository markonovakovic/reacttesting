import { types } from './types';
import axios from 'axios';

export const fetchPosts = () => async (dispatch) => {
    try {
    const result =  await axios.get('https://jsonplaceholder.typicode.com/posts?_limit=10')
        dispatch({
            type: types.GET_POSTS,
            payload: result.data
        })
    }
    catch(err) {
        // console.log(err);
    }
};