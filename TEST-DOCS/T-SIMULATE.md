```javascript

  describe('logic', () => {
    const wrapper = shallow(<Navbar  links={links} />);
    wrapper.find('.login-btn').simulate('click');

    it('btn click - update title', () => {
      const text = wrapper.find('.navbar-brand').text()
      expect(text).toEqual('Tralala')
    })
  })
  
```

## Change Event:
```javascript

  it('Should call onChange function', () => {
    const onChange = jest.fn();
    const value = 'Hello'
    const wrapper = shallow(<Input onChange={onChange}  value={value} style={{width:'300px', padding:'4px'}}/>);
    wrapper.find('input').simulate('change');
    

    expect(onChange).toHaveBeenCalled()
  })
```

## Simulate event:
### Component ```Button.js```:
```javascript
test('Should handle onClick handler', () => {
   const Button = ({onClick,title, ...props}) => {
      return (
          <div className="button-component">
                  <button onClick={onClick} {...props}> { title || 'Show Table'}</button>
          </div>
      )
   }

```
### Component ```Button.test.js```:
```javascript
test('Should handle onClick handler', () => {
    const onClickSpy = jest.fn();
    const wrapper = shallow(<Button onClick={onClickSpy} />);
    wrapper.find('button').simulate('click');

    expect(onClickSpy).toHaveBeenCalled();

})
```