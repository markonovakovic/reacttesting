## Setup Coverage Test Report

1) Create document ***jest.config.json***
   
```js
   {
        "setupFiles": ["<rootDir>/src/setupTests.js"],
        "testRegex": "/*.test.js$",
        "collectCoverage": true,
        "coverageReporters": [
            "lcov"
        ],
        "coverageDirectory": "test-coverage",
        "coverageThreshold": {
            "global": {
                "branches": 0,
                "functions": 0,
                "lines": 0,
                "statements": 0
            }
        },
        "moduleDirectories": [
            "node_modules",
            "src"
        ]
    }
```

Here we are telling jest to generate coverage reports for all files ending with .test.js as shown intestRegex property.

### Execute the following command from terminal: 
```
 npm run test -- --coverage --watchAll=false 
 ```

***Once executed, we will see the total coverage for each test file:***
  
![alt text](./img/run-coverage.png)