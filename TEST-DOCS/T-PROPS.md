# Props

```javascript
  describe('Passing props', () => {
        const headlineProps = {
            title:"helo man",
            description:'Test'
        }
        const headlineWrapper = mount(<Headline title={headlineProps.title} />)
        it('accepts header title props', () => {
           expect(headlineWrapper.props().title).toEqual(headlineProps.title)
        })
    })

```

## Triger events from child component:

#### Component ```Navbar.js```
```javascript
   const [ navbarTitle, setNavbarTitle] = useState('ReactTest')
   <Button className="moje dugme" text="Click" handleClick={() => setNavbarTitle('Sahbaz')} />
   return(
         <h2 data-test="navbar-title" className="navbar-brand">{navbarTitle}</h2>
   )
```
#### Test ```Navbar.test.js```:
```javascript
  it('should click Button component and update title', () => {
      const wrapper =  shallow(<Navbar links={links} />)
      const button = wrapper.find('Button');
 
      button.invoke('handleClick')()
      const text = wrapper.find('.navbar-brand').text();
      expect(text).toEqual('fafafa');
      
    
    })
```