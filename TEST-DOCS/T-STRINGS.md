### test text of element

1) Option :
```javascript
  it('render text of Navbar title', () => {
    const wrapper = shallow(<App />);
    const text = wrapper.find(`[data-test='navbar-title'`).text();
   // const text = findByTestAttr(wrapper, 'navbar-title').text()

    expect(text).toEqual('My React App')
  })

```
2) Option :
```javascript
 it('renders Nav component header without crashing', () => {
    const wrapper = shallow(<App />);
    const header = (<h2 data-test="navbar-title" className="navbar-brand">My React App</h2>);
    expect(wrapper.contains(header)).toEqual(true)
  })

```