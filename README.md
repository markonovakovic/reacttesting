# Testing App

## Installal packages
- Install  ```npm install --save-dev enzyme react-test-renderer``` 
  ***Notice!***
  For react version > 17 you have to install next enzyme adapter 
   ```npm install --save-dev @wojtekmaj/enzyme-adapter-react-17```
   
  https://www.npmjs.com/package/@wojtekmaj/enzyme-adapter-react-17
 

## Setup
Create setupTest.js file into src folder ```touch src/setupTest.js``` with next code:

  ```javascript
   import Enzyme from 'enzyme';
   import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

    Enzyme.configure({
        adapter: new Adapter(),
        disableLifecycleMethods:true
        });
  ```